# Strand environments
Strands allows the user to set the global enviroment of the strand function when creating a new strand.
The usefulness is that this allows to call some functions (e.g., the wait functions) to be called as if they were global from within the strand, without actually polluting the global environment.

## From Strand.new/Strand.start

When creating a strand directly through the @{strands.Strand} class, the function's environment will not be changed unless an environment table is passed explicitly. 

## From StrandRegistry.create/StrandRegistry.start

When creating a strand through a @{strands.StrandRegistry}, the function's enviroment will by default be changed to the environment containing the wait functions and with an `__index` methamethod pointing to `_G`, so that the regular global environment can be access as well.

To use a different environment or to avoid changing the environment at all @{strands.StrandRegistry:set_environment} should be used.

## What to use as an environment?

Normally you would either not change it at all, or use the default one obtained from @{strands.get_default_environment}. If you want to have more than just the wait functions in the environment you can either add them to the default environment (the table only contains the documented wait functions, so there's no risk of overwriting important things), or create a new one add add the wait functions to it.

## Cons of using an environment

Changing the environment after the function in Lua 5.2+ is not something that is generally recommended and can only be done through the @{debug} library. If this is something you would rather not do then you can make sure the environment is not changed by removing the registry environment with @{strands.StrandRegistry:set_environment}.

I personally like being able to access the wait functions as global within the strands more than I dislike using the debug library, but if you don't the library provides you with options.
