------------------------------------------------------------------------------------
-- MIT License                                                                    --
------------------------------------------------------------------------------------
--                                                                                --
-- Copyright (c) 2024 apicici                                                     --
--                                                                                --
-- Permission is hereby granted, free of charge, to any person obtaining a copy   --
-- of this software and associated documentation files (the "Software"), to deal  --
-- in the Software without restriction, including without limitation the rights   --
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      --
-- copies of the Software, and to permit persons to whom the Software is          --
-- furnished to do so, subject to the following conditions:                       --
--                                                                                --
-- The above copyright notice and this permission notice shall be included in all --
-- copies or substantial portions of the Software.                                --
--                                                                                --
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     --
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       --
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    --
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         --
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  --
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  --
-- SOFTWARE.                                                                      --
------------------------------------------------------------------------------------

--[[-- A pure Lua library to manage co-operative multitasking in games.
Used to declare a lists of sequential things for the game to do (including waiting),
that can be done in parallel in the background.

Designed for point & click adventure games, but can be used in any genre.
Also includes facilities for event handling through signals.

See @{strands_sample.lua}, @{strands_say.lua}, @{strands_nested.lua},
and @{strands_signal.lua}
for examples of usage.

See @{01-strand_environments.md|Strand environments} for more details about
how and whether to use strand environments.

@module strands
@alias M
]]

local M = {}
--- Library name.
M._NAME="strands"
--- Library version.
M._VERSION="0.1.0"

local signals
local Strand
local StrandRegistry
local strand_env

local current_strand


-- ----------
-- setfenv --
-- ----------

local getupvalue = debug.getupvalue
local setupvalue = debug.setupvalue
local upvaluejoin = debug.upvaluejoin

-- recreate (limited) setfenv if using Lua 5.2+
local setfenv = _G.setfenv or function(f, t)
    -- note: f is assumed to be a function
    -- find _ENV upvalue
    local up = 1
    local name
    while true do
        name = getupvalue(f, up)
        if not name then
            error("Cannot find _ENV upvalue.", 3)
        elseif name == "_ENV" then
            break
        end
        up = up + 1
    end
    
    -- point upvalue somewher else
    local tmp = function() return up end
    upvaluejoin(f, up, tmp, 1)
    setupvalue(f, up, t)
    return f
end

M._setfenv = setfenv


--- Check if the function invoking this is running inside a strand.

function M.inside_strand()
    return not not current_strand
end

--- Get the table corresponding to the default strand environment.
-- This  contains the [wait functions](#Wait_functions).

function M.get_default_environment()
    return strand_env
end


--[[-- Strands are threads of execution that use coroutines to wrap a function
and allow to pause execution inside it as necessary.
@type Strand
]]


Strand = {}
M.Strand = Strand

--[[-- Create new strand.
It is recommended to create strands using @{StrandRegistry:create} instead.
@see Strand.start, StrandRegistry:create
@raise `f` must be a function
@func f function containing the commands to execute in the strand. Should not have any arguments
@tparam ?table env environment to load global functions from, within `f`.
If not provided no changes are made to the function environment.
--]]

function Strand.new(f, env)
    assert(type(f) == "function", "Expected function as argument.")
    if env then
        setfenv(f, env)
    end
    local t = {
        _co = coroutine.create(f),
        update = Strand.update,
        stop = Strand.stop,
        resume = Strand.resume,
        active = Strand.active,
        _next = nil,
        _prev = nil
    }
    return setmetatable(t, Strand)
end

--[[-- Create and automatically start new strand.
It is recommended to create and start strands using @{StrandRegistry:start} instead.
@see Strand.new, StrandRegistry:start
@param f same as @{Strand.new}
@param[opt] env same as @{Strand.new}
]]

function Strand.start(f, env)
    local t = Strand.new(f, env)
    t:resume()
    return t
end

--[[-- Update the strand.
Meant to be called when updating the game loop, should normally be done at every frame.

If the strand is updated and reaches the end of its execution the [signal](#signals)
with the strand as object and `"finished"` as the identifier is emitted.

**Note**: don't update strands directly if they are already being updated
through @{StrandRegistry:update}.
@number dt time elapsed since last update. Units can be chosen by the user,
but have to be consistent with the ones used in @{wait}
@raise If any error is raised inside the strand
]]

function Strand:update(dt)
    if self._dead then return end
    local old_strand = current_strand
    current_strand = self
    assert(coroutine.resume(self._co, dt))
    if coroutine.status(self._co) == "dead" then
        self._dead = true
        signals.emit(self, "finished")
    end
    current_strand = old_strand
end

--[[-- Stop the strand.
After calling this method the strand can no longer be updated.
Use one of the [wait functions](#Wait_functions) to temporarily pause the strand.
]]

function Strand:stop()
    self._dead = true
    if current_strand == self then coroutine.yield() end
end

--[[-- Resume a paused strand.
Same as calling `Strand:update` with `dt=0`.
Convenience function for starting a thread created with `Strand.new`.
]]

function Strand:resume()
    self:update(0)
end

--[[-- Check if a strand is active.
@treturn bool `true` if the strand has not reached the end of its associated function and has not been stopped with @{Strand:update}
]]

function Strand:active()
    return not self._dead
end


--[[-- A container that can start multiple strands and update them all at the same time.
@type StrandRegistry
]]

StrandRegistry = {}
M.StrandRegistry = StrandRegistry

--[[-- Create new strand registry.
The registry is assigned the default strand environment (the one returned by @{get_default_environment} containing the [wait functions](#Wait_functions).
The environment can be changed using @{StrandRegistry:set_environment}.
@treturn StrandRegistry strand registry instance
]]

function StrandRegistry.new()
    local t = {
        _first = nil,
        _last = nil,
        _env = strand_env,
        create = StrandRegistry.create,
        start = StrandRegistry.start,
        update = StrandRegistry.update,
        clear = StrandRegistry.clear,
        set_environment = StrandRegistry.set_environment,
        get_environment = StrandRegistry.get_environment,
        is_running = StrandRegistry.is_running
    }
    return setmetatable(t, StrandRegistry)
end

--[[-- Create a new strand and add it to the registry.
The strand is created by passing the current registry environment
to @{Strand.new}.
@see StrandRegistry:start, StrandRegistry:set_environment, Strand.new
@func f function associated to the strand. Should not have any arguments
@treturn Strand strand instance
]]

function StrandRegistry:create(f)
    local s = Strand.new(f, self._env)
    if self._last then
        self._last._next = s
        s._prev = self._last
        self._last = s
    else
        self._first = s
        self._last = s
    end
    return s
end

--[[-- Create and start a new strand and add it to the registry.
The strand is created by passing the current registry environment
to @{Strand.new}.
@see StrandRegistry:create, StrandRegistry:set_environment, Strand.start
@func f same as @{StrandRegistry.new}
@treturn Strand same as @{StrandRegistry.new}
]]

function StrandRegistry:start(f)
    local s = self:create(f)
    s:resume()
    do return s end
end

--[[-- Update all registered strands.
Meant to be called when updating the game loop, should normally be done at every frame.
If a strand has reached the end or has been stopped with @{Strand:stop} it
is removed from the registry.

Strands are updated in the order they were created.

If a strand is updated and reaches the end of its execution the [signal](#signals)
with the strand as object and `"finished"` as the identifier is emitted.
@number dt same as @{Strand:update}
]]

function StrandRegistry:update(dt)
    local s = self._first
    while s do
        if s._dead then
            local p, n = s._prev, s._next
            if p then p._next = n end
            if n then n._prev = p end
            if s == self._first then self._first = n end
            if s == self._last then self._last = p end
        else
            s:update(dt)
        end
        s = s._next
    end
end

--[[-- De-register all active strands.
The strands can still be updated individually through the instances
obtained when they were created.
]]

function StrandRegistry:clear()
    self._first = nil
    self._last = nil
end

--[[-- Set the registry environment.
@see StrandRegistry:get_environment
@tparam ?table|nil env table to use for the registry environment. If the registry environment
is set to nil, the registry will pass a nil environment to @{Strand.new} when creating new strands.
]]

function StrandRegistry:set_environment(env)
    self._env = env
end

--[[-- Get the registry environment.
Unless changed, the default environment is the one returned by @{get_default_environment}
and containing the [wait functions](#Wait_functions),
@treturn ?table|nil current environment (`nil` if no environment is set)
]]

function StrandRegistry:get_environment()
    return self._env
end

--[[-- Check if a strand from this registry is currently running.
@treturn bool `true` if a strand from this registry is running, `false` otherwise
]]

function StrandRegistry:is_running()
    return not not self._strands[current_strand]
end


--[[-- Wait functions.
These functions can normally only be accessed as fields of the table returned
by @{get_default_environment}. They can be accessed as global functions from
within a strand if it was created with an environment that cointains them
(for example if they are created from a registry that has the default environment set).
@section wait_functions
]]

strand_env = setmetatable({}, {__index=_G})

local yield = coroutine.yield

--[[-- Pause strand execution for a specified amount of time.
@function wait
@number t the amount of time to wait. The units can be chosen arbitrarily,
but should be consistent with @{Strand:update} and @{StrandRegistry:update}
@treturn number how much actual time has elapsed
]]

function strand_env.wait(t)
    local s = 0
    while s < t do
        s = s + yield()
    end
    return(s)
end

--[[-- Pause strand execution until the next update.
Normally the strands should be updated every frame,
which results in a pause of a single frame. Useful to
release control from the strand in a loop that should
do something each frame.
@function wait_frame
@treturn number duration of the wait (effectively the `dt` passed when updating the strand)
]]

function strand_env.wait_frame()
    return yield()
end

--[[-- Pause strand execution until another strand has finished running (or was stopped).
@function wait_strand
@tparam Strand strand to wait for
@treturn number how much actual time has elapsed
]]

function strand_env.wait_strand(strand)
    local t = 0
    while not strand._dead do t = t + yield() end
    return t
end

--[[-- Pause strand execution until a signal is emitted.
@function wait_signal
@param object object of the signal
@string identifier of the signal
@treturn number how much actual time has elapsed
@see signals.emit
]]

function strand_env.wait_signal(object, name)
    local t = 0
    local loop = true
    local f = function()
        loop = false
    end
    signals.connect(object, name, f)
    while loop do t = t + yield() end
    signals.disconnect(object, name, f)
    return t
end

local floor = math.floor

local function iter(s, i)
    if s.last then return end
    while s.accum < s.step_duration do
        s.accum = s.accum + yield()
    end
    local k = i + floor(s.accum/s.step_duration)
    s.accum = s.accum%s.step_duration
    if k > s.num_steps then
        if not s.loop then
            if i == s.num_steps then return end
            s.last = true
            return s.num_steps
        end
        k = (k - 1)%s.num_steps + 1
    end
    return k
end

--[[-- Produces an iterator that cycles through integers by waiting a specified time between steps.
To be used in a generic for loop.
Starts iterating from 1. Useful to use strands to control animations.

**Note**: to ensure that exactly the right amount of time is spent in each step, the iterator can skip over some
steps, effectively returning the index of the steps that you would get by waiting the current total elapsed time.
If not looping, the last index (`num_steps`) is always returned before ending the iterator.
  
@usage
for i in accumulator(0.5, 10) do
    print(i)
end

@usage
-- roughly equivalent to the much more cumbersome

local step_duration, num_steps = 0.5, 10
local duration = num_steps*step_duration
local t = 0
local accum = 0
while t < duration do
    local i = math.min(math.floor(t/step_duration) + 1, num_steps)
    print(i)
    while accum < step_duration do
        accum = accum + wait_frame()
    end
    t = t + accum
    accum = accum%step_duration
end

@function accumulator
@number duration duration of the step. The units should be chosen consistently with @{Strand:update}
@number num_steps total number of steps to iterato over. It can be set to @{math.huge} to leave it uncapped
@tparam ?bool loop `true` if the itarator should restart
@treturn function the iterator
@treturn table table containing the state of the iterator
@treturn int the number 0
]]

function strand_env.accumulator(step_duration, num_steps, loop)
    local s = {
        step_duration = step_duration,
        num_steps = num_steps,
        accum = step_duration,
        loop = loop or false
    }
    return iter, s, 0
end


--[[-- Signals.
Functions for event handling through emission of signals.
@section signals
]]

signals = {}
M.signals = signals

local signals_table = setmetatable({}, {__mode = "k"})

--[[-- Register a function to be called when a signal is emitted.

@param object object the signal is associated to (e.g., the player). Can be of any type other than `nil`
@string name identifier of the event to signal (e.g., "finished moving"). Different objects can have the same identifier without conflicts
@func f function to register
]]

function signals.connect(object, name, f)
    if not signals_table[object] then
        signals_table[object] = {}
    end
    local t = signals_table[object]
    if not t[name] then
        t[name] = {}
    end
    t[name][f] = true
end

--[[-- Disconnect a function from a signal.
@param object same as @{signals.connect}
@string name identifier same as @{signals.connect}
@func f identifier same as @{signals.connect}
]]

function signals.disconnect(object, name, f)
    local t = signals_table[object]
    local s = t and t[name]
    if not s then return end
    s[f] = nil
    if next(s) == nil then
        t[name] = nil
        if next(t) == nil then
            signals_table[object] = nil
        end
    end
end

--[[-- Emit a signal. Calls all connected functions.

**Note**: no guarantees are made on the order in which the connected functions will be called.
@param object object the signal is associated to (e.g., the player). Can be of any type other than `nil`
@string name identifier of the event to signal (e.g., "finished moving"). Different objects can have the same identifier without conflicts
@param ... arguments to pass to the registered functions when calling them
]]

function signals.emit(object, name, ...)
    local t = signals_table[object]
    local s = t and t[name]
    if not s then return end
    for f in pairs(s) do
        f(...)
    end
end

--- Clear all registered functions.

function signals.clear()
    signals_table = setmetatable({}, {__mode = "k"})
end


return M
