------------------------------------------------
-- example using LÖVE (but as a pure Lua library
-- strands can be used with other game engines)
------------------------------------------------

-------------------------------------------------------
-- strand that waits until the user click on the screen
-------------------------------------------------------

local strands = require("strands")
local registry = strands.StrandRegistry.new()


-- update, draw, & input
------------------------

-- update registry when updating the game loop
function love.update(dt)
    registry:update(dt)
end

local clicked = false

function love.draw()
    if clicked then
        love.graphics.print("Clicked!")
    end
end

function love.mousepressed()
    if not clicked then
        strands.signals.emit("game", "clicked")
    end
end

--  strand
----------

-- when signal is emitted set clicked to true, but
-- only for 2 seconds

registry:start(function()
    while true do
        wait_signal("game", "clicked")
        clicked = true
        wait(2)
        clicked = false
    end
end)