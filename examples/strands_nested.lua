------------------------------------------------
-- example using LÖVE (but as a pure Lua library
-- strands can be used with other game engines)
------------------------------------------------

-------------------------------
-- example using nested strands
-------------------------------

local strands = require("strands")
local registry = strands.StrandRegistry.new()

-- update registry when updating the game loop
function love.update(dt)
    registry:update(dt)
end

registry:start(function()
    local s1 = registry:start(function ()
        print("first started")
        wait(2)
        print("first finished")
    end)
    local s2 = registry:start(function ()
        print("second started")
        wait(3)
        print("second finished")
    end)

    -- nested threads run in parallel of the current one,
    -- so we keep going
    print("----")
    print("both started")
    print("----")

    -- wait for threads to finish
    while s1:active() or s2:active() do wait_frame() end
    print("----")
    print("both finished")
end)