---------------------------------------------------
-- example using LÖVE (but as a pure Lua library --
-- strands can be used with other game engines)  --
---------------------------------------------------

local strands = require("strands")
local registry = strands.StrandRegistry.new()

-- update registry when updating the game loop
function love.update(dt)
    registry:update(dt)
end

-- draw a letter or number on the screen
local letters = "abcdefghijklmnopqrstuvz0123456789"
local idx = 1
function love.draw()
    love.graphics.print(letters:sub(idx, idx))
end

registry:start(function()
    -- change screen colour in a loop
    local speed = .5
    while true do
        local intensity = 0

        -- change from black to grey
        while intensity < 0.5 do
            local dt = wait_frame()
            intensity = math.min(0.5, intensity + speed*dt)
            love.graphics.setBackgroundColor(intensity, intensity, intensity)
        end

        -- wait before changing back
        wait(1)

        -- change from grey to black
        while intensity > 0 do
            local dt = wait_frame()
            intensity = math.max(0, intensity - speed*dt)
            love.graphics.setBackgroundColor(intensity, intensity, intensity)
        end

         -- wait before restarting the loop 
        wait(1)
    end
end)

-- change the currently drawn character by looping throught the
-- list at rate of 2 characters per second
registry:start(function()
    for i in accumulator(0.5, #letters, true) do
        idx = i
    end
end)