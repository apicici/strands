------------------------------------------------
-- example using LÖVE (but as a pure Lua library
-- strands can be used with other game engines)
------------------------------------------------

--------------------------------------
-- conversation between two characters
--------------------------------------

local strands = require("strands")
local registry = strands.StrandRegistry.new()


-- facilities for displaying character dialogue
-----------------------------------------------

-- extract wait function so it can be used in other functions
local wait = strands.get_default_environment().wait

local current_text
local speed = 10 -- letters per second
local function say(character, text)
    current_text = string.format("%s: %s", character, text)
    local duration = math.max(1.5, #text/speed)
    wait(duration)
    -- clear text once the character has finished speaking
    current_text = nil
    -- wait a bit before next line of dialogue
    wait(.2)
end

-- update & draw
----------------

-- update registry when updating the game loop
function love.update(dt)
    registry:update(dt)
end

-- draw dialogue, if any
function love.draw()
    if current_text then
        love.graphics.print(current_text) 
    end
end

-- conversation strand
----------------------

registry:start(function()
    wait(0.5)
    say("Alice", "Hi Bob.")
    say("Bob", "Oh hi, Alice.")
    say("Alice", "What a useful library `strands` is.")
    say("Bob", "Indeed!")
end)