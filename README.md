# Strands

A pure Lua library to manage co-operative multitasking in games.

Used to declare a lists of sequential things for the game to do (including waiting), that can be done in parallel in the background.

Designed for point & click adventure games, but can be used in any genre. Also includes facilities for event handling through signals.

## Usage

Grab the latest release and put the `strands.lua` somewhere that can be found using `require`. Refer to the documentation (inside the release or [online](https://apicici.codeberg.page/strands/)) to learn how to use the library.